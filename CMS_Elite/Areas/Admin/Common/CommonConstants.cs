﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS_Elite.Common
{
    public class CommonConstants
    {        
        public static string USER_SESSION = "USER_SESSION";
        public static string ACCOUNT_SESSION = "ACCOUNT_SESSION";
        public static string MENU_SESSION = "MENU_SESSION";
        public static string SESSION_CREDENTIALS = "SESSION_CREDENTIALS";
        public static string ANH_SESSION = "ANH_SESSION";
        public static string MA_NVIEN_SESSION = "MA_NVIEN_SESSION";
        public static string PASSWORD = "PASSWORD";
        public static string CartSession = "CartSession";
        public static string CurrentCulture { set; get; }
    }
}