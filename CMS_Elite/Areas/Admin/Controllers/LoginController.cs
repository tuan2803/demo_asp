﻿using CMS_Elite.Common;
using CMS_Elite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace CMS_Elite.Areas.Admin.Controllers
{
    public class LoginController : Controller
    {
        #region Variables
        CMSEliteDbContext db = new CMSEliteDbContext();
        public static string key = "tuan";
        #endregion
        // GET: Admin/Login
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(tblAccount model)
        {
            var pass = Encrypt(model.Password);
            var item = db.tblAccounts.Where(x => x.AccountName == model.AccountName && x.Password == pass && x.Status == 1).FirstOrDefault();
            if (item != null)
            {
                //Co
                if (item.DateIssued < DateTime.Now)
                {
                    Session[CommonConstants.ACCOUNT_SESSION] = item;
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Tài khoản hết hiệu lực");
                    return View();
                }
            }
            else
            {
                //Khong co
                ModelState.AddModelError("", "Thông tin tài khoản hoặc mật khẩu không chính xác");
                return View();
            }
        }
        public static string Encrypt(string toEncrypt)
        {
            bool useHashing = true;
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);

            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }
    }
}