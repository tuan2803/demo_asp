﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CMS_Elite.Areas.Admin.Controllers
{
    public class HomeController : Controller
    {
        // GET: Admin/Home
        public ActionResult Index()
        {
            //ViewBag.Breadcum = "Home";
            return View();
        }
        public JsonResult GetChart1()
        {
            //var lstData = new List<Chart1>();
            var rd = new Random();
            List<object> iData = new List<object>();
            List<object> iData2 = new List<object>();
            for (int i = 0; i <= 6; i++)
            {
                var x = new Object();
                x = rd.Next(10, 100);
                iData.Add(x);

                var y = new Object();
                y = rd.Next(10, 100);
                iData2.Add(y);
            }


            return Json(new { data = iData, data2 = iData2 }, JsonRequestBehavior.AllowGet);
        }
        public class clsChart1
        {
            public string val1 { get; set; }
            public string val2 { get; set; }
        }

        private class Chart1
        {
        }
    }
}