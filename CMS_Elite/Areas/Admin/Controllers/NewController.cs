﻿using CMS_Elite.Controllers;
using CMS_Elite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CMS_Elite.Areas.Admin.Controllers
{
    public class NewController : BaseController
    {
        #region Variable
        CMSEliteDbContext db;
        #endregion
        // GET: Admin/New
        public ActionResult Index()
        {
            db = new CMSEliteDbContext();
            var model = db.tblNews.Where(x => x.Status == 1).ToList();
            return View(model);
        }

        // GET: Admin/New/Details/5
        public ActionResult Details(int id)
        {

            SetCombobox();
            db = new CMSEliteDbContext();
            var model = db.tblNews.Find(id);
            var item = db.tblCategories.Where(x=>x.Status==1);
            Session["cate"] = item;
            return View(model);
        }

        // GET: Admin/New/Create
        public ActionResult Create()
        {
            tblNew model = new tblNew();
            model.PostDate = DateTime.Now;
            SetCombobox();
            return View(model);
        }

        public void SetCombobox()
        {
            db = new CMSEliteDbContext();
            var model = db.tblCategories.Where(x => x.Status == 1).OrderBy(x => x.OrderNumber).ToList();
            ViewBag.Category = model;
        }

        // POST: Admin/New/Create
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Create(tblNew model, HttpPostedFileBase filePost1, HttpPostedFileBase filePost2, FormCollection collection, string theloai)
        {

            string fileLocation1 = "";
            string fileLocation2 = "";
            if (Request.Files["filePost1"].ContentLength <= 0 && Request.Files["filePost2"].ContentLength <= 0)
            {
                model.ImageLarge = "";
                model.ImageThumb = "";
            }
            model.ImageLarge = "../assets/node_modules/dropify/src/images/test-image-1.jpg";
            model.ImageThumb = "../assets/node_modules/dropify/src/images/test-image-1.jpg";
           
            if (Request.Files["filePost1"].ContentLength > 0 && Request.Files["filePost2"].ContentLength > 0)
            {
                string filename1 = System.IO.Path.GetFileName(filePost1.FileName);
                fileLocation1 = Server.MapPath("~/Content/") + DateTime.Now.ToString("dd-MM-yyyy HH-mm-ss") + filename1;
                filePost1.SaveAs(fileLocation1);

                string filename2 = System.IO.Path.GetFileName(filePost2.FileName);
                fileLocation2 = Server.MapPath("~/Content/") + DateTime.Now.ToString("dd-MM-yyyy HH-mm-ss") + filename2;
                filePost2.SaveAs(fileLocation2);

                if (System.IO.File.Exists(fileLocation1) && System.IO.File.Exists(fileLocation2))
                {
                    System.IO.File.Delete(fileLocation1);
                    System.IO.File.Delete(fileLocation2);
                }
                Request.Files["filePost1"].SaveAs(fileLocation1);
                Request.Files["filePost2"].SaveAs(fileLocation2);
            }

            model.CateNewsID = Convert.ToInt32(collection["cboCategory"].ToString());
            model.Title = model.Title;
            model.Author = model.Author;
            model.ShortDescribe = model.ShortDescribe;
            model.PostDate = model.PostDate;
            if (theloai == "Tin hót")
            {
                model.IsHot = 1;
                model.IsView = 0;
                model.IsComment = 0;
            }
            else if (theloai == "Tin mới")
            {
                model.IsHot = 0;
                model.IsView = 1;
                model.IsComment = 0;
            }
            else if (theloai == "Bình luận")
            {
                model.IsHot = 0;
                model.IsView = 0;
                model.IsComment = 1;
            }
            //var text= model.FullDescribe.Replace("<p>", " ");
            //model.FullDescribe = text.Replace("</p>", " ").Trim();
            int iContent1 = fileLocation1.IndexOf("Content");
            int iContent2 = fileLocation2.IndexOf("Content");
            if (iContent1 > 0)
            {
                model.ImageLarge = @"\" + fileLocation1.Substring(iContent1, fileLocation1.Length - iContent1);
            }
            if (iContent2 > 0)
            {
                model.ImageThumb = @"\" + fileLocation2.Substring(iContent2, fileLocation2.Length - iContent2);
            }
            model.Language = 0;
            model.IsApproval = 0;
            model.CommentTotal = 0;
            model.Status = 1;
            model.CreatedDate = DateTime.Now;
            model.CreatedUser = "tuan";
            model.ModifiedDate = DateTime.Now;
            model.ModifiedUser = "tuan";
            model.ApprovalDate = DateTime.Now;
            model.ApprovalUser = "tuan";
            ModelState["CreatedUser"].Errors.Clear();
            ModelState["ModifiedUser"].Errors.Clear();

            if (ModelState.IsValid == true)
            {
                db = new CMSEliteDbContext();
                db.tblNews.Add(model);
                var Result = db.SaveChanges();
                if (Result == 1)
                {
                    SetAlert("success", "Ghi dữ liệu thành công");
                    return RedirectToAction("Index", "New");
                }
            }
            else
            {
                ModelState.AddModelError("", "Chưa nhập đầy đủ thông tin");
            }
            return View();
        }

        // GET: Admin/New/Edit/5
        public ActionResult Edit(int id)
        {
            SetCombobox();
            db = new CMSEliteDbContext();
            var model= db.tblNews.Find(id);
            return View(model);
        }

        // POST: Admin/New/Edit/5
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Edit(tblNew model, HttpPostedFileBase filePost1, HttpPostedFileBase filePost2, FormCollection collection, string theloai)
        {

            db = new CMSEliteDbContext();
            var item = db.tblNews.Where(x => x.ID == model.ID).SingleOrDefault();
            
            string fileLocation1 = "";
            string fileLocation2 = "";
            //if (Request.Files["filePost1"].ContentLength <= 0 && Request.Files["filePost2"].ContentLength <= 0)
            //{
            //    model.ImageLarge = "";
            //    model.ImageThumb = "";
            //}
            //model.ImageLarge = "../assets/node_modules/dropify/src/images/test-image-1.jpg";
            //model.ImageThumb = "../assets/node_modules/dropify/src/images/test-image-1.jpg";

            if (Request.Files["filePost1"].ContentLength > 0 && Request.Files["filePost2"].ContentLength > 0)
            {
                string filename1 = System.IO.Path.GetFileName(filePost1.FileName);
                fileLocation1 = Server.MapPath("~/Content/") + DateTime.Now.ToString("dd-MM-yyyy HH-mm-ss") + filename1;
                filePost1.SaveAs(fileLocation1);

                string filename2 = System.IO.Path.GetFileName(filePost2.FileName);
                fileLocation2 = Server.MapPath("~/Content/") + DateTime.Now.ToString("dd-MM-yyyy HH-mm-ss") + filename2;
                filePost2.SaveAs(fileLocation2);

                //if (System.IO.File.Exists(fileLocation1) && System.IO.File.Exists(fileLocation2))
                //{
                //    System.IO.File.Delete(fileLocation1);
                //    System.IO.File.Delete(fileLocation2);
                //}
                //Request.Files["filePost1"].SaveAs(fileLocation1);
                //Request.Files["filePost2"].SaveAs(fileLocation2);

                int iContent1 = fileLocation1.IndexOf("Content");
                int iContent2 = fileLocation2.IndexOf("Content");
                if (iContent1 > 0)
                {
                    item.ImageLarge = @"\" + fileLocation1.Substring(iContent1, fileLocation1.Length - iContent1);
                }
                if (iContent2 > 0)
                {
                    item.ImageThumb = @"\" + fileLocation2.Substring(iContent2, fileLocation2.Length - iContent2);
                }
            }
            else
            {
                item.ImageLarge = model.ImageLarge;
                item.ImageThumb = model.ImageThumb;
            }

            item.CateNewsID = Convert.ToInt32(collection["cboCategory"].ToString());
            item.Title = model.Title;
            item.Author = model.Author;
            item.ShortDescribe = model.ShortDescribe;
            item.PostDate = model.PostDate;
            if (theloai == "Tin hót")
            {
                item.IsHot = 1;
                item.IsView = 0;
                item.IsComment = 0;
            }
            else if (theloai == "Tin mới")
            {
                item.IsHot = 0;
                item.IsView = 1;
                item.IsComment = 0;
            }
            else if (theloai == "Bình luận")
            {
                item.IsHot = 0;
                item.IsView = 0;
                item.IsComment = 1;
            }
            //var text= model.FullDescribe.Replace("<p>", " ");
            //model.FullDescribe = text.Replace("</p>", " ").Trim();

            item.Language = 0;
            item.IsApproval = 0;
            item.CommentTotal = 0;
            item.Status = 1;
            //item.CreatedDate = DateTime.Now;
            //item.CreatedUser = "tuan";
            item.ModifiedDate = DateTime.Now;
            item.ModifiedUser = "tuan";
            item.FullDescribe = model.FullDescribe;
            //item.ApprovalDate = DateTime.Now;
            item.ApprovalUser = "tuan";
            ModelState["CreatedUser"].Errors.Clear();
            ModelState["ModifiedUser"].Errors.Clear();

            var uResult = db.SaveChanges();
            if (uResult >= 0)
            {
                SetAlert("success", "Cập nhật dữ liệu thành công");
                return RedirectToAction("Index", "New");
            }
            else
            {
                SetAlert("error", "Lỗi trong quá trình ghi dữ liệu");
                return View();
            }
        }

        // GET: Admin/New/Delete/5
        public ActionResult Delete(int id)
        {

            try
            {
                db = new CMSEliteDbContext();
                //var ID = Convert.ToInt32(id);
                var item = db.tblNews.Find(id);
                if (item != null)
                {
                    item.Status = 0;
                    db.SaveChanges();
                }

                return RedirectToAction("Index", "New");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }

        // POST: Admin/New/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            return View();
        }
    }
}
