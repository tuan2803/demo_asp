﻿using CMS_Elite.Controllers;
using CMS_Elite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CMS_Elite.Areas.Admin.Controllers
{
    public class RoleController : BaseController
    {
        #region Variable
        CMSEliteDbContext db;
        #endregion
        // GET: Admin/Role
        public ActionResult Index()
        {
            db = new CMSEliteDbContext();
            var mode = db.tblRoles;
            return View(mode);
        }
        public ActionResult Create()
        {
            return PartialView();
        }
        [HttpPost]
        public ActionResult Create(tblRole model, FormCollection collection)
        {
            model.CreatedDate = DateTime.Now;
            model.CreatedUser = "tuansoi";
            model.ModifiedDate = DateTime.Now;
            model.ModifiedUser = "tuansoi";
            model.Status = 1;
            ModelState["CreatedUser"].Errors.Clear();
            ModelState["ModifiedUser"].Errors.Clear();

            if (ModelState.IsValid == true)
            {
                db = new CMSEliteDbContext();
                db.tblRoles.Add(model);
                db.SaveChanges();
                return RedirectToAction("Index", "Role");
            }
            return View();
        }
        public ActionResult Edit(string id)
        {
            db = new CMSEliteDbContext();
            var model = db.tblRoles.Find(id);
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult Edit(tblRole model, FormCollection collection)
        {

            ModelState["CreatedUser"].Errors.Clear();
            ModelState["ModifiedUser"].Errors.Clear();

            if (ModelState.IsValid == true)
            {
                db = new CMSEliteDbContext();
                var item = db.tblRoles.Find(model.RoleID);
                item.RoleName = model.RoleName;
                item.Description = model.Description;
                item.ModifiedDate = DateTime.Now;
                item.ModifiedUser = "tuansoi";
                item.Status = Convert.ToInt32(collection["cboMenu"].ToString());
                db.SaveChanges();
                return RedirectToAction("Index", "Role");
            }
            return View();
        }
        public ActionResult Details(string id)
        {
            db = new CMSEliteDbContext();
            var model = db.tblRoles.Find(id);
            return PartialView(model);
        }

        public ActionResult Delete(string id)
        {
            try
            {
                db = new CMSEliteDbContext();
                //var ID = Convert.ToInt32(id);
                var item = db.tblRoles.Find(id);
                if (item != null)
                {
                    db.tblRoles.Remove(item);
                    db.SaveChanges();
                }

                return RedirectToAction("Index", "Role");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }
    }
}