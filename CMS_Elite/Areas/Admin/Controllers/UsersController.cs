﻿using CMS_Elite.Controllers;
using CMS_Elite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace CMS_Elite.Areas.Admin.Controllers
{
    public class UserController : BaseController
    {
        #region variable
        public static string key = "tuan";
        #endregion
        // GET: Admin/User
        public ActionResult Index()
        {
            CMSEliteDbContext db = new CMSEliteDbContext();
            var model = (from a in db.tblAccounts
                         where a.Status == 1
                         join b in db.tblUsers
                         on a.ID equals b.Account_ID
                         select new
                         {
                             objAccount = a,
                             objUser = b
                         }
                        ).ToList();
            List<tblAccountUser> lstAU = new List<tblAccountUser>();
            foreach (var item in model)
            {
                tblAccountUser ac = new tblAccountUser();
                ac.AccountName = item.objAccount.AccountName;
                ac.Account_ID = item.objAccount.ID;
                ac.Address = item.objUser.Address;
                ac.Birthday = item.objUser.Birthday;
                ac.CreatedDate = item.objUser.CreatedDate;
                ac.CreatedUser = item.objUser.CreatedUser;
                ac.DateIssued = item.objAccount.DateIssued;
                ac.Description = item.objUser.Description;
                ac.ModifiedDate = item.objAccount.ModifiedDate;
                ac.ModifiedUser = item.objAccount.ModifiedUser;
                ac.Password = item.objAccount.Password;
                ac.Sex = item.objUser.Sex;
                ac.UserName = item.objUser.UserName;
                ac.ImageUrl = item.objUser.ImageUrl;
                lstAU.Add(ac);
            }
            return View(lstAU);
        }

        [HttpGet]
        public ActionResult Create()
        {
            tblAccountUser model = new tblAccountUser();
            model.DateIssued = DateTime.Now;
            model.Birthday = DateTime.Now.AddYears(-21);
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(tblAccountUser model, HttpPostedFileBase filePost)
        {
            string fileLocation = "";
            if (Request.Files["filePost"].ContentLength <= 0) { model.ImageUrl = ""; }
            model.ImageUrl = "../assets/node_modules/dropify/src/images/test-image-1.jpg";
            //ModelState["filePost"].Errors.Clear();
            if (ModelState.IsValid == true)
            {
                if (Request.Files["filePost"].ContentLength > 0)
                {
                    string filename = System.IO.Path.GetFileName(filePost.FileName);
                    fileLocation = Server.MapPath("~/Content/") +DateTime.Now.ToString("dd-MM-yyyy HH-mm-ss") +filename;
                    filePost.SaveAs(fileLocation);

                    if (System.IO.File.Exists(fileLocation))
                    {
                        System.IO.File.Delete(fileLocation);
                    }
                    Request.Files["filePost"].SaveAs(fileLocation);
                }

                tblAccount accItem = new tblAccount();
                accItem.AccountName = model.AccountName;
                accItem.CreatedDate = DateTime.Now;
                accItem.CreatedUser = "tuan";
                accItem.DateIssued = model.DateIssued;
                accItem.ID = -1;
                accItem.ModifiedDate = DateTime.Now;
                accItem.ModifiedUser = "tuan";

                var pass = Encrypt(model.Password);

                accItem.Password = pass;
                accItem.Status = 1;
                CMSEliteDbContext db = new CMSEliteDbContext();
                db.tblAccounts.Add(accItem);
                db.SaveChanges();
                tblUser userItem = new tblUser();
                userItem.Account_ID = accItem.ID;
                userItem.Address = model.Address;
                userItem.Birthday = model.Birthday;
                userItem.CreatedDate = DateTime.Now;
                userItem.CreatedUser = "anhpd";
                userItem.Description = model.Description;
                userItem.ID = -1;

                int iContent = fileLocation.IndexOf("Content");
                if (iContent > 0)
                {
                    userItem.ImageUrl = @"\" + fileLocation.Substring(iContent, fileLocation.Length - iContent);
                }

                //userItem.ImageUrl = @"\" + fileLocation;
                userItem.ModifiedDate = DateTime.Now;
                userItem.ModifiedUser = "anhpd";
                userItem.Sex = 1;
                userItem.Status = 1;
                userItem.UserName = model.UserName;
                db.tblUsers.Add(userItem);
                var Result = db.SaveChanges();
                if (Result == 1)
                {
                    SetAlert("success", "Ghi dữ liệu thành công");
                    return RedirectToAction("Index", "User");
                }

                return View();
            }
            else
            {
                ModelState.AddModelError("", "Chưa nhập đầy đủ thông tin");
            }
            return View();
        }

        [HttpGet]
        public ActionResult Edit(int ID)
        {
            //Lay thong tin tai khoan nguoi dung
            CMSEliteDbContext db = new CMSEliteDbContext();
            var model = (from a in db.tblAccounts
                         join b in db.tblUsers
                         on a.ID equals b.Account_ID
                         where a.ID == ID
                         select new
                         {
                             objAccount = a,
                             objUser = b
                         }
                        ).SingleOrDefault();
            tblAccountUser item = new tblAccountUser();
            item.AccountName = model.objAccount.AccountName;
            item.Account_ID = model.objAccount.ID;
            item.Address = model.objUser.Address;
            item.Birthday = model.objUser.Birthday;
            item.CreatedDate = model.objUser.CreatedDate;
            item.CreatedUser = model.objUser.CreatedUser;
            item.DateIssued = model.objAccount.DateIssued;
            item.Description = model.objUser.Description;
            item.ID = model.objUser.ID;
            item.ImageUrl = model.objUser.ImageUrl;
            item.ModifiedDate = model.objUser.ModifiedDate;
            item.ModifiedUser = model.objUser.ModifiedUser;
            item.Password = Decrypt(model.objAccount.Password);
            item.Sex = model.objUser.Sex;
            item.Status = 1;
            item.UserName = model.objUser.UserName;

            return View(item);
        }

        [HttpPost]
        public ActionResult Edit(tblAccountUser item, HttpPostedFileBase filePost)
        {
            CMSEliteDbContext db = new CMSEliteDbContext();
            var acc = db.tblAccounts.Find(item.Account_ID);
            acc.AccountName = item.AccountName;
            acc.CreatedDate = item.CreatedDate;
            acc.CreatedUser = item.CreatedUser;
            acc.DateIssued = item.DateIssued;
            acc.ModifiedDate = DateTime.Now;
            acc.ModifiedUser = "anhpd";
            acc.Password = Encrypt(item.Password);
            acc.Status = 1;

            var uResult = db.SaveChanges();
            var user = db.tblUsers.Where(x => x.Account_ID == item.Account_ID).SingleOrDefault();
            string fileLocation;
            if (Request.Files["filePost"].ContentLength > 0)
            {
                string filename = System.IO.Path.GetFileName(filePost.FileName);
                fileLocation = Server.MapPath("~/Content/") + DateTime.Now.ToString("dd-MM-yyyy HH-mm-ss") + filename;
                filePost.SaveAs(fileLocation);
                int iContent = fileLocation.IndexOf("Content");
                if (iContent > 0)
                {
                    user.ImageUrl = @"\" + fileLocation.Substring(iContent, fileLocation.Length - iContent);
                }
            }
            else
            {
                user.ImageUrl = item.ImageUrl;
            }




            user.ModifiedDate = DateTime.Now;
            user.ModifiedUser = "anhpd";
            user.Sex = item.Sex;
            user.Status = 1;
            user.UserName = item.UserName;
            user.Birthday = item.Birthday;
            db.SaveChanges();

            if (uResult >= 0)
            {
                SetAlert("success", "Cập nhật dữ liệu thành công");
                return RedirectToAction("Index", "User");
            }
            else
            {
                SetAlert("error", "Lỗi trong quá trình ghi dữ liệu");
                return View();
            }

        }

        public ActionResult Delete(int ID)
        {
            try
            {
                CMSEliteDbContext db = new CMSEliteDbContext();
                var acc = db.tblAccounts.Where(x => x.ID == ID).SingleOrDefault();
                acc.Status = 0;
                db.SaveChanges();
                var user = db.tblUsers.Where(x => x.Account_ID == ID).SingleOrDefault();
                user.Status = 0;
                var Result = db.SaveChanges();
                if (Result >= 0)
                {
                    SetAlert("success", "Xóa dữ liệu thành công");
                }
                else
                {
                    SetAlert("error", "Lỗi trong quá trình xóa dữ liệu");
                }
                return RedirectToAction("Index", "User");
            }
            catch (Exception ex)
            {
                SetAlert("error", ex.Message);
                return RedirectToAction("Index", "User");
            }
        }
        public ActionResult Details(int ID)
        {
            CMSEliteDbContext db = new CMSEliteDbContext();
            var model = (from a in db.tblAccounts
                         join b in db.tblUsers
                         on a.ID equals b.Account_ID
                         where a.ID == ID
                         select new
                         {
                             objAccount = a,
                             objUser = b
                         }
                        ).SingleOrDefault();
            tblAccountUser item = new tblAccountUser();
            item.AccountName = model.objAccount.AccountName;
            item.Account_ID = model.objAccount.ID;
            item.Address = model.objUser.Address;
            item.Birthday = model.objUser.Birthday;
            item.CreatedDate = model.objUser.CreatedDate;
            item.CreatedUser = model.objUser.CreatedUser;
            item.DateIssued = model.objAccount.DateIssued;
            item.Description = model.objUser.Description;
            item.ID = model.objUser.ID;
            item.ImageUrl = model.objUser.ImageUrl;
            item.ModifiedDate = model.objUser.ModifiedDate;
            item.ModifiedUser = model.objUser.ModifiedUser;
            item.Password = model.objAccount.Password;
            item.Sex = model.objUser.Sex;
            item.Status = 1;
            item.UserName = model.objUser.UserName;

            return PartialView(item);
        }
        public static string Encrypt(string toEncrypt)
        {
            bool useHashing = true;
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);

            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        public static string Decrypt(string toDecrypt)
        {
            bool useHashing = true;
            byte[] keyArray;
            byte[] toEncryptArray = Convert.FromBase64String(toDecrypt);

            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            return UTF8Encoding.UTF8.GetString(resultArray);
        }
    }
}