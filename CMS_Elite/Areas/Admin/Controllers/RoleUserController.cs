﻿using CMS_Elite.Controllers;
using CMS_Elite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CMS_Elite.Areas.Admin.Controllers
{
    public class RoleUserController : BaseController
    {
        #region Variables
        CMSEliteDbContext db;
        #endregion
        // GET: Admin/RoleUser
        public ActionResult Index()
        {
            SetTable();
            return View();
        }

        public JsonResult SaveData(string ID, String Choice)
        {
            //Thực hiện cập nhật dữ liệu
            var strID = ID.Replace("chk_", "").Replace(" ", "").Trim();
            var arrID = strID.Split('_');
            var UserID = arrID[0].ToString();
            var RoleID = arrID[1].ToString();
            db = new CMSEliteDbContext();
            if (Choice.ToUpper() == "TRUE")
            {
                //Thực hiện ghi dữ liệu
                var item = new tblUser_Role();
                item.ID = -1;
                item.UserID = Convert.ToInt32(RoleID);
                item.RoleID = UserID;

                db.tblUser_Role.Add(item);
                var result = db.SaveChanges();
                if (result > 0)
                {
                    return Json("OK", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                //Xóa dữ liệu
                var uID = Convert.ToInt32(RoleID);
                var item = db.tblUser_Role.Where(x => x.RoleID == UserID && x.UserID == uID).SingleOrDefault();
                var Result = db.tblUser_Role.Remove(item);
                db.SaveChanges();
                if (Result != null)
                {
                    return Json("KO", JsonRequestBehavior.AllowGet);
                }
            }
            return Json("OK", JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        //public ActionResult Index(FormCollection collection)
        //{
        //    SetTable();

        //    //Xóa dữ liệu
        //    for (int i = 1; i < collection.AllKeys.Count(); i++)
        //    {
        //        if (collection.AllKeys[i].Contains('_') == true)
        //        {
        //            var arr = collection.AllKeys[i].ToString().Split('_');
        //            string MA_NVIEN = arr[2].ToString().Trim();
        //        }
        //    }
        //    //Ghi dữ liệu
        //    string resInsert = "";
        //    for (int i = 1; i < collection.AllKeys.Count(); i++)
        //    {
        //        if (collection.AllKeys[i].Contains("_") == true)
        //        {
        //            var arr = collection.AllKeys.ToString().Split('_');
        //            string MA_QUYEN = arr[1].ToString().Trim();
        //            string MA_NVIEN = arr[2].ToString().Trim();
        //        }
        //    }

        //    if (resInsert == "OK")
        //    {
        //        //SetAlert("Success", "Thêm mới dữ liệu thành công");
        //        return RedirectToAction("Index", "RoleUser");
        //    }
        //    else
        //    {
        //        //SetAlert("Error", "Lỗi trong quá trình thêm dữ liệu:" + resInsert);
        //        return View();
        //    }
        //}

        #region Public funtions
        private void SetTable()
        {
            try
            {
                db = new CMSEliteDbContext();
                var lstRole = db.tblRoles.ToList();
                Session["Role"] = lstRole;

                var lstUser = db.tblUsers.ToList();
                Session["User"] = lstUser;

                var lstQuyen = (from a in db.tblUsers
                                join b in db.tblUser_Role
                                on a.ID equals b.UserID
                                where a.Status == 1
                                select new
                                {
                                    ID = a.ID,
                                    UserName = a.UserName,
                                    RoleID = b.RoleID,
                                }).ToList();
                List<tblUser_Role> iUserRole = new List<tblUser_Role>();
                foreach (var item in lstQuyen)
                {
                    tblUser_Role iNew = new tblUser_Role();
                    iNew.ID = item.ID;
                    iNew.RoleID = item.RoleID;
                    iNew.UserID = item.ID;
                    iUserRole.Add(iNew);
                }
                Session["User_Role"] = iUserRole;
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                throw;
            }
        }
        #endregion
    }
}
