﻿using CMS_Elite.Controllers;
using CMS_Elite.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace CMS_Elite.Areas.Admin.Controllers
{
    public class MenuController : BaseController
    {
        #region Variables
        CMSEliteDbContext db;
        #endregion
        // GET: Admin/Menu
        public ActionResult Index()
        {
            db = new CMSEliteDbContext();
            var model = db.tblMenus.Where(x => x.TypeMenu == 0 && x.Status == 1).ToList();
            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            SetCombobox();
            return View();
        }

        [HttpPost]
        public ActionResult Create(tblMenu model, FormCollection collection)
        {
            model.TypeMenu = 0;
            model.Location = "-1";
            model.CategoryID = -1;
            model.NewsID = -1;
            model.Url = "-1";
            model.Parent_ID = Convert.ToInt32(collection["cboMenu"].ToString());
            model.Page = "-1";
            ModelState["Location"].Errors.Clear();
            model.Status = 1;
            if (ModelState.IsValid == true)
            {
                db = new CMSEliteDbContext();
                db.tblMenus.Add(model);
                var result = db.SaveChanges();

                return RedirectToAction("Index", "Menu");
            }
            else
            {
                ModelState.AddModelError("", "Lỗi kiểm tra dữ liệu");
                SetCombobox();
                return View();
            }
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            SetCombobox();
            db = new CMSEliteDbContext();
            var model = db.tblMenus.Find(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(tblMenu model, FormCollection collection)
        {
            SetCombobox();
            try
            {
                ModelState["Location"].Errors.Clear();
                if (ModelState.IsValid == true)
                {
                    db = new CMSEliteDbContext();
                    var item = db.tblMenus.Find(model.ID);
                    item.MenuName = model.MenuName;
                    item.UserControl = model.UserControl;
                    if (collection["cboMenu"].ToString().Trim() != "")
                        item.Parent_ID = Convert.ToInt32(collection["cboMenu"].ToString());
                    else
                        item.Parent_ID = -1;
                    item.MetaTitle = model.MetaTitle;
                    item.OrderNumber = model.OrderNumber;
                    db.SaveChanges();
                }

                return RedirectToAction("Index", "Menu");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }
        public void SetCombobox()
        {
            db = new CMSEliteDbContext();
            var model = db.tblMenus.Where(x => x.TypeMenu == 0 && x.Status == 1).ToList();
            ViewBag.Menu = model;
        }

        public ActionResult Modal()
        {
            return PartialView();
        }
        public ActionResult Details(int id)
        {
            db = new CMSEliteDbContext();
            var model = db.tblMenus.Find(id);
            return PartialView(model);
        }
        public ActionResult Delete(int id)
        {
            try
            {
                db = new CMSEliteDbContext();
                //var ID = Convert.ToInt32(id);
                var item = db.tblMenus.Find(id);
                if (item != null)
                {
                    //db.tblMenus.Remove(item);
                    item.Status = 0;
                    db.SaveChanges();
                }
                SetAlert("success", "Xóa dữ liệu thành công");
                return RedirectToAction("Index", "Menu");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }
    }
}