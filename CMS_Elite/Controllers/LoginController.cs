﻿using CMS_Elite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace CMS_Elite.Controllers
{
    public class LoginController : Controller
    {
        #region Variables
        CMSEliteDbContext db = new CMSEliteDbContext();
        public static string key = "tuan";
        #endregion
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(FormCollection userlog)
        {
           
            string userMail = userlog["AccountName"].ToString();
            string password = userlog["Password"].ToString();
            var pass = Encrypt(password);
            //var islogin = db.tblAccounts.SingleOrDefault(x => x.AccountName.Equals(userMail) && x.Password.Equals(pass));
            var islogin = (from a in db.tblAccounts
                         where a.Status == 1 && a.AccountName == userMail && a.Password == pass
                         join b in db.tblUsers
                         on a.ID equals b.Account_ID
                         select new
                         {
                             objAccount = a,
                             objUser = b
                         }
                        ).FirstOrDefault();

            if (islogin != null)
            {
                //if (userMail == "Admin@gmail.com")
                //{
                //    Session["use"] = islogin;
                //    return RedirectToAction("Index", "Admin/Home");
                //}
                //else
                //{
                    Session["use"] = (CMS_Elite.Models.tblUser)islogin.objUser;
                    return RedirectToAction("Index", "Home");
                //}
            }
            else
            {
                Session["use"] = (CMS_Elite.Models.tblUser)null;
                ModelState.AddModelError("", "Thông tin tài khoản hoặc mật khẩu không chính xác");
                ViewBag.Fail = "Đăng nhập thất bại";
                return RedirectToAction("Index", "Home");
            }

        }
        [HttpPost]
        public ActionResult SignUp(FormCollection model)
        {

            string Name = model["Name"].ToString();
            string Email = model["Email"].ToString();
            string password = model["password"].ToString();
            string Confirm_Password = model["Confirm Password"].ToString();

            
       
            //ModelState["filePost"].Errors.Clear();
            

                tblAccount accItem = new tblAccount();
                accItem.AccountName = Name;
                accItem.CreatedDate = DateTime.Now;
                accItem.CreatedUser = "tuan";
                accItem.DateIssued = DateTime.Now;
                accItem.ID = -1;
                accItem.ModifiedDate = DateTime.Now;
                accItem.ModifiedUser = "tuan";

                var pass = Encrypt(password);

                accItem.Password = pass;
                accItem.Status = 1;
                CMSEliteDbContext db = new CMSEliteDbContext();
                db.tblAccounts.Add(accItem);
                db.SaveChanges();
                tblUser userItem = new tblUser();
                userItem.Account_ID = accItem.ID;
                userItem.Address = "";
                userItem.Birthday = DateTime.Now;
                userItem.CreatedDate = DateTime.Now;
                userItem.CreatedUser = "anhpd";
                userItem.Description = "";
                userItem.ID = -1;



                userItem.ImageUrl = "../assets/node_modules/dropify/src/images/test-image-1.jpg";
                userItem.ModifiedDate = DateTime.Now;
                userItem.ModifiedUser = "anhpd";
                userItem.Sex = 1;
                userItem.Status = 1;
                userItem.UserName = Name;
                db.tblUsers.Add(userItem);
                var Result = db.SaveChanges();
                if (Result == 1)
                {
                    
                    return RedirectToAction("Index", "Home");
                }

            else
            {
                ModelState.AddModelError("", "Chưa nhập đầy đủ thông tin");
            }
            return View();
        }

        

        public static string Encrypt(string toEncrypt)
        {
            bool useHashing = true;
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);

            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }
        public ActionResult Logout()
        {
            Session["use"] = null;
            return RedirectToAction("Index", "Home");

        }
    }
}