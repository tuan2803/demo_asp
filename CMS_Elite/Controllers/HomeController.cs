﻿using CMS_Elite.Models;
using CMS_Elite.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CMS_Elite.Controllers
{
    public class HomeController : Controller
    {
        CMSEliteDbContext db = null;
        public ActionResult Index()
        {
            return View();
        }


        [ChildActionOnly]
        [OutputCache(Duration = 3600 * 24)]
        public ActionResult LoginModal()
        {
            return PartialView();
        }

        [ChildActionOnly]
        [OutputCache(Duration = 3600 * 24)]
        public ActionResult SingUp()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult check(FormCollection tt)
        {
            return View();
        }

    }
}