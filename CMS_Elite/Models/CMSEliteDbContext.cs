namespace CMS_Elite.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class CMSEliteDbContext : DbContext
    {
        public CMSEliteDbContext()
            : base("name=CMSEliteDbContext")
        {
        }

        public virtual DbSet<tblAccount> tblAccounts { get; set; }
        public virtual DbSet<tblAdv> tblAdvs { get; set; }
        public virtual DbSet<tblAlbum> tblAlbums { get; set; }
        public virtual DbSet<tblBanner> tblBanners { get; set; }
        public virtual DbSet<tblCategory> tblCategories { get; set; }
        public virtual DbSet<tblImage> tblImages { get; set; }
        public virtual DbSet<tblMenu> tblMenus { get; set; }
        public virtual DbSet<tblMenu_Role> tblMenu_Role { get; set; }
        public virtual DbSet<tblModulePanel> tblModulePanels { get; set; }
        public virtual DbSet<tblNew> tblNews { get; set; }
        public virtual DbSet<tblNews_File> tblNews_File { get; set; }
        public virtual DbSet<tblRole> tblRoles { get; set; }
        public virtual DbSet<tblTypeModule> tblTypeModules { get; set; }
        public virtual DbSet<tblUser> tblUsers { get; set; }
        public virtual DbSet<tblUser_Role> tblUser_Role { get; set; }
        public virtual DbSet<tblVideo> tblVideos { get; set; }
        public virtual DbSet<tblWebsiteLink> tblWebsiteLinks { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<tblImage>()
                .Property(e => e.SizeImage)
                .HasPrecision(18, 3);
        }
    }
}
